<?php
/**
 * Plugin Name: Website Cloner
 * Plugin URI: https://datdora.com/
 * Description: A plugin to clone and download a website as a ZIP file.
 * Version: 1.0
 * Author: Đạt Nguyễn JX
 * Author URI: https://datdora.com/
 */
// Hàm website_cloner_shortcode(): Đây là hàm dùng để tạo shortcode cho plugin. Hàm này sẽ tạo một biểu mẫu với các trường đầu vào và thanh tiến trình để cho phép người dùng sao chép trang web và tải xuống file ZIP. Hàm này cũng thực hiện kết nối đến REST API để xử lý sao chép trang web.

// Hàm website_cloner_rest_route(): Hàm này sẽ đăng ký một REST API endpoint để xử lý yêu cầu sao chép trang web.

// Hàm website_cloner_callback(): Đây là hàm xử lý yêu cầu sao chép trang web được gửi đến REST API endpoint. Hàm này sẽ kiểm tra xem URL có hợp lệ hay không và tiến hành sao chép trang web. Sau khi hoàn thành sao chép, hàm này sẽ tạo một tập tin ZIP và gửi nó đến trình duyệt của người dùng.

//Hàm clone_website_and_create_zip(): Hàm này sẽ sao chép trang web và tạo file ZIP. Hàm này sử dụng thư viện "Goutte" và "Symfony DomCrawler" để tải xuống tất cả các tài nguyên (CSS, JS, hình ảnh, v.v.) của trang web và thay thế các URL trong HTML bằng các đường dẫn địa phương. Sau đó, hàm này sử dụng ZipArchive để tạo một file ZIP từ các tài nguyên đã tải xuống.

// Hàm rrmdir(): Đây là một hàm trợ giúp để xóa một thư mục và tất cả các tệp con bên trong.
require_once('inc/website_cloner_shortcode.php');
function website_cloner_shortcode($atts) {
	
    ob_start();

    ?>
<form method="post" id="website_cloner_form">
    <label for="website_url">Website URL:</label>
    <input type="url" name="website_url" id="website_url" required>
    <label for="zip_filename">Zip filename:</label>
    <input type="text" name="zip_filename" id="zip_filename">
    <input type="submit" value="Clone">
</form>
<br>
<div id="progress-bar">
  <div id="progress-bar-fill"></div>
  <div id="progress-bar-text">0%</div>
</div>
<style>
input#website_url {
    border-radius: 12px;
    background: #828c91;
    width: 50%;
}
#progress-bar {
	border-radius: 12px;
  width: 100%;
  height: 30px;
  background-color: #7e6269;
  position: relative;
}

#progress-bar-fill {
  height: 100%;
  background-color: #0073aa;
  width: 0%;
  transition: width 0.3s ease-in-out;
}

#progress-bar-text {
  position: absolute;
  top: 0;
  left: 50%;
  transform: translate(-50%, 0);
  color: #fff;
  font-weight: bold;
}

</style>
<script>
function downloadWebsite() {
    // Lấy giá trị URL từ người dùng
    var url = document.getElementById('website_url').value;
    // Kiểm tra xem URL có hợp lệ hay không
    if (url) {
        // Tạo đường dẫn API và tải về file zip
        var downloadLink = document.createElement('a');
        downloadLink.href = '/wp-json/website_cloner/v1/clone?website_url=' + encodeURIComponent(url);
        downloadLink.download = 'clone.zip';
        downloadLink.click();
        // Xoá cache của trình duyệt
        window.location.reload(true);
    }
}
    // Thay đổi tên file zip trong thanh tải về trình duyệt
    var zipFilename = '<?php echo isset($_POST['zip_filename']) ? sanitize_file_name($_POST['zip_filename']) : 'clone.zip'; ?>';
    var downloadLink = document.createElement('a');
    downloadLink.href = '/wp-json/website_cloner/v1/clone?website_url=' + encodeURIComponent(url);
    downloadLink.download = zipFilename;
    downloadLink.click();
</script>
    <script>
        document.getElementById('website_cloner_form').addEventListener('submit', function(event) {
            event.preventDefault();
            var url = document.getElementById('website_url').value;
            if (url) {
                window.location.href = '/wp-json/website_cloner/v1/clone?website_url=' + encodeURIComponent(url);
            }
        });
		document.getElementById('website_cloner_form').addEventListener('submit', function(event) {
  event.preventDefault();
  var url = document.getElementById('website_url').value;
  if (url) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', '/wp-json/website_cloner/v1/clone?website_url=' + encodeURIComponent(url));
    xhr.onloadstart = function() {
      document.getElementById('progress-bar-fill').style.width = '0% Vui Lòng Đợi';
      document.getElementById('progress-bar-text').textContent = '0% Vui Lòng Đợi';
    };
    xhr.onprogress = function(event) {
      if (event.lengthComputable) {
        var percentComplete = (event.loaded / event.total) * 80;
        document.getElementById('progress-bar-fill').style.width = percentComplete + '%';
        document.getElementById('progress-bar-text').textContent = percentComplete.toFixed(0) + '%';
      }
    };
    xhr.onloadend = function() {
      document.getElementById('progress-bar-fill').style.width = '99%';
      document.getElementById('progress-bar-text').textContent = '100%';
    };
    xhr.send();
  }
});
    </script>
	
    <?php
    return ob_get_clean();
}
add_shortcode('website_cloner', 'website_cloner_shortcode');
require_once __DIR__ . '/vendor/autoload.php';
function website_cloner_rest_route() {
    register_rest_route('website_cloner/v1', '/clone', array(
        'methods' => 'GET',
        'callback' => 'website_cloner_callback',
    ));
}
add_action('rest_api_init', 'website_cloner_rest_route');
// Thêm function vào file functions.php trong theme

function website_cloner_callback($data) {
    // Kiểm tra URL hợp lệ
    if (!isset($data['website_url']) || !filter_var($data['website_url'], FILTER_VALIDATE_URL)) {
        wp_die('Invalid URL.');
        return;
    }

    $website_url = $data['website_url'];

    // Clone the website and create a ZIP file.
    // Please note that the implementation of this function is beyond the scope of this example.
    // Make sure you have the necessary permissions and legal rights before cloning a website.
    $zip_file_path = clone_website_and_create_zip($website_url);

    if (!$zip_file_path) {
        // Show error message if ZIP file cannot be created
        wp_die('An error occurred while creating the ZIP file.');
    }

    if (!file_exists($zip_file_path)) {
        // Show error message if ZIP file does not exist
        wp_die('The ZIP file could not be found.');
    }

    // Lấy tên file zip từ tham số mới
    $zip_filename = isset($data['zip_filename']) ? sanitize_file_name($data['zip_filename']) : 'clone.zip';

    header('Content-Type: application/zip');
    header('Content-Disposition: attachment; filename="' . $zip_filename . '"');
    header('Content-Length: ' . filesize($zip_file_path));

    // Trả về file zip
    readfile($zip_file_path);
    exit;
}

// Sử dụng function để thông báo các tiến trình và lỗi trong quá trình tạo file zip


use Goutte\Client;
use Symfony\Component\DomCrawler\Crawler;

function clone_website_and_create_zip($url) {
    $client = new Client();
    $crawler = $client->request('GET', $url);
$asset_urls = array();
    // Create a temporary directory for the downloaded files
    $download_path = sys_get_temp_dir() . '/website_clone_' . uniqid();
    mkdir($download_path);

    // Create the necessary directories
    mkdir($download_path . '/css', 0755, true);
    mkdir($download_path . '/js', 0755, true);
    mkdir($download_path . '/img', 0755, true);
    mkdir($download_path . '/assets', 0755, true);

    // Save the main HTML file
    $dom = new DOMDocument();
    libxml_use_internal_errors(true);
    $dom->loadHTML($crawler->html());
    libxml_clear_errors();
    $dom->formatOutput = true;
    $dom->preserveWhiteSpace = false;

    $formatted_html = $dom->saveHTML();

    // Replace the URLs of CSS files
    $css_files = $crawler->filter('link[rel="stylesheet"]')->each(function ($node) {
        return $node->attr('href');
    });

    foreach ($css_files as $css_file) {
        $css_url = $css_file;
        $css_filename = basename($css_url);
        $local_css_path = 'css/' . preg_replace('/(\?|&)ver=[0-9\.]+/', '', $css_filename);
        $formatted_html = str_replace($css_url, $local_css_path, $formatted_html);
        $css_content = file_get_contents($css_url);
        file_put_contents($download_path . '/' . $local_css_path, $css_content);
    }

    // Replace the URLs of JS files
    $js_files = $crawler->filter('script[src]')->each(function ($node) {
        return $node->attr('src');
    });

    foreach ($js_files as $js_file) {
        $js_url = $js_file;
        $js_filename = basename($js_url);
        $local_js_path = 'js/' . preg_replace('/(\?|&)ver=[0-9\.]+/', '', $js_filename);
        $formatted_html = str_replace($js_url, $local_js_path, $formatted_html);
        $js_content = file_get_contents($js_url);
        file_put_contents($download_path . '/' . $local_js_path, $js_content);
    }

    // Replace the URLs of image files
    $image_files = $crawler->filter('img[src]')->each(function ($node) {
        return $node->attr('src');
    });

    foreach ($image_files as $image_file) {
        $image_url = $image_file;
        $image_filename = basename($image_url);
        $local_image_path = 'img/' . $image_filename;
        $formatted_html = str_replace($image_url, $local_image_path, $formatted_html);
        $image_content = file_get_contents($image_url);
        file_put_contents($download_path . '/' . $local_image_path, $image_content);
    }

    // Download asset files
$asset_urls = array_unique($asset_urls);
foreach ($asset_urls as $asset_url) {
    // Get the file name and extension
    $file_name = basename(parse_url($asset_url, PHP_URL_PATH));
    $file_extension = pathinfo($file_name, PATHINFO_EXTENSION);

    // Set the local file path
    $local_file_path = 'assets/' . uniqid() . '.' . $file_extension;
    $local_file_path = str_replace(array(' ', '%20'), '_', $local_file_path);

    // Download the asset file
    $ch = curl_init($asset_url);
    $fp = fopen($download_path . '/' . $local_file_path, 'wb');
    curl_setopt($ch, CURLOPT_FILE, $fp);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_exec($ch);
    curl_close($ch);
    fclose($fp);

    // Replace the asset URL with the local file path in the formatted HTML
    $formatted_html = str_replace($asset_url, $local_file_path, $formatted_html);
}
// Save the formatted HTML
file_put_contents($download_path . '/index.html', $formatted_html);

// Create a ZIP archive of the downloaded files
$zip = new ZipArchive();
$zip_filename = $download_path . '.zip';

if ($zip->open($zip_filename, ZipArchive::CREATE) === TRUE) {
    $files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($download_path), RecursiveIteratorIterator::LEAVES_ONLY);

    foreach ($files as $file) {
        if (!$file->isDir()) {
            $file_path = $file->getRealPath();
            $file_relative_path = substr($file_path, strlen($download_path) + 1);
            $zip->addFile($file_path, $file_relative_path);
        }
    }

    $zip->close();
}

// Delete the temporary directory
$files = new RecursiveIteratorIterator(new RecursiveDirectoryIterator($download_path, FilesystemIterator::SKIP_DOTS), RecursiveIteratorIterator::CHILD_FIRST);

foreach ($files as $fileinfo) {
    if ($fileinfo->isDir()) {
        rmdir($fileinfo->getRealPath());
    } else {
        unlink($fileinfo->getRealPath());
    }
}

rmdir($download_path);

return $zip_filename;
}

// Helper function to delete a directory and its contents
function rrmdir($dir) {
    if (is_dir($dir)) {
        $objects = scandir($dir);
        foreach ($objects as $object) {
            if ($object != "." && $object != "..") {
                if (is_dir($dir . DIRECTORY_SEPARATOR . $object) && !is_link($dir . "/" . $object)) {
                    rrmdir($dir . DIRECTORY_SEPARATOR . $object);
                } else {
                    unlink($dir . DIRECTORY_SEPARATOR . $object);
                }
            }
        }
        rmdir($dir);
    }
}